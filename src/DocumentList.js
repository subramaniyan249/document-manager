import { React, useState } from "react";
import { Tree, Row, Col, PageHeader } from "antd";
import './DocumentList.css';
import DocumentListDetails from './DocumentListDetails';

const DocumentList = () => {
  const [user, setUserData]=useState({"keys" : ["user-001"]});
  const { DirectoryTree } = Tree;
  const treeData = [
    {
      title: "USERS",
      key: "0-0",
      children: [
        {
          title: "User 1",
          key: "user-001",
          isLeaf: true,
        },
        {
          title: "User 2",
          key: "user-002",
          isLeaf: true,
        },
      ],
    },
    {
      title: "PROFILE",
      key: "0-1",
      children: [
        {
          title: "Profile 1",
          key: "prof-010",
          isLeaf: true,
        },
        {
          title: "Profile 2",
          key: "prof-020",
          isLeaf: true,
        },
        {
          title: "Profile 3",
          key: "prof-021",
          isLeaf: true,
        },
        {
          title: "Profile 4",
          key: "prof-022",
          isLeaf: true,
        },
        {
          title: "Profile 5",
          key: "prof-023",
          isLeaf: true,
        },
        {
          title: "Profile 6",
          key: "prof-024",
          isLeaf: true,
        }
      ],
    },
  ];
  const onSelect = (keys, info) => {
    console.log("Trigger Select", keys);
    setUserData({...user, keys}) 
  };

  const onExpand = () => {
    console.log("Trigger Expand");
  };

  return (
    <div className="document-container">
      <PageHeader className="site-page-header" title="Document manager" />
      <Row className="document-row"> 
        <Col span={4} className="document-left-panel">
          <DirectoryTree 
            multiple
            defaultExpandAll
            defaultSelectedKeys={["user-001"]}
            onSelect={onSelect}
            onExpand={onExpand}
            treeData={treeData}
          />
        </Col>
        <Col span={20}>
            <DocumentListDetails userId={user}>

            </DocumentListDetails>
        </Col>
      </Row>
    </div>
  );
};

export default DocumentList;
