import axios from "axios";

const axiosInstance = axios.create({
    baseURL: "https://eos.eoxvantage.com",
    headers : {
        "content-type": "application/json"
    }
})

export default axiosInstance;