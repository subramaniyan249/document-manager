import { React, useState } from "react";
import { FolderOutlined, FolderFilled } from "@ant-design/icons";
import { Row, Col } from "antd";
import './DocumentListDetails.css';

const DocumentListDetails = ({ userId }) => {
  const [theArray, setTheArray] = useState([]);
  const folderData = {
    "user-001": [
      {
        id: "001-1",
        DocumentName: "Organization_1",
      },
      {
        id: "001-2",
        DocumentName: "UserDetails_1",
      },
      {
        id: "001-3",
        DocumentName: "letters_1",
      },
      {
        id: "001-4",
        DocumentName: "Promotion_1",
      },
    ],
    "user-002": [
      {
        id: "002-1",
        DocumentName: "Organization_2",
      },
      {
        id: "002-2",
        DocumentName: "UserDetails_2",
      },
      {
        id: "002-3",
        DocumentName: "letters_2",
      },
      {
        id: "002-4",
        DocumentName: "Promotion_2",
      },
    ],
    "prof-010": [
      {
        id: "002-1",
        DocumentName: "Organization_2",
      },
      {
        id: "002-2",
        DocumentName: "UserDetails_2",
      },
      {
        id: "002-3",
        DocumentName: "letters_2",
      },
      {
        id: "002-4",
        DocumentName: "Promotion_2",
      },
    ],
    "prof-020": [
      {
        id: "002-1",
        DocumentName: "Organization_2",
      },
      {
        id: "002-2",
        DocumentName: "UserDetails_2",
      },
      {
        id: "002-3",
        DocumentName: "letters_2",
      },
      {
        id: "002-4",
        DocumentName: "Promotion_2",
      },
    ]
  };
  let filteredData = [];
  Object.entries(folderData).forEach((entry) => {
    const [key, value] = entry;
    if (userId) {
      if (key === userId["keys"][0]) {
        filteredData.push(value);
      }
    }
  });
  filteredData = filteredData[0];

  return (
    <div>
      <Row>
        {filteredData ? (
          filteredData.map((document, index) => (
            <div className="each-folder">
              <Col span={8}>
                <FolderFilled style={{ fontSize: "5rem", color: "#40a9ff" }} />
              </Col>
              <div key={index}>{document.DocumentName}</div>
            </div>
          ))
        ) : (
          <div></div>
        )}
      </Row>
    </div>
  );
};

export default DocumentListDetails;
