import "./Login.css";
import { Input, Space, PageHeader, Button } from "antd";
import "antd/dist/antd.css";
import React, {  useState } from 'react';
import axiosInstance from './axios';
import {  useHistory } from 'react-router-dom';


const Login = () =>{
  const [formData, setFormData] = useState({});
  let history = useHistory();

  const handleChange = (e) => {
    setFormData({...formData, [e.target.name]: e.target.value})
  }

  const loginUser = () => {
    axiosInstance.post('/login', formData)
      .then(response => {
        history.push("/DocumentList")
      })
      .catch((err) => console.log(err))
  }

  return (
    <div className="Login">
      <Space direction="vertical">
        <PageHeader className="site-page-header" title="EOX" />
        <Input name="username" placeholder="Username" onChange={handleChange}/>
        <Input.Password name="password" placeholder="input password" onChange={handleChange}/>
        <Button type="primary" onClick={loginUser}>
          Login
        </Button>
      </Space>
    </div>
  );
}

export default Login
